//
//  SecondViewController.swift
//  MVPTesting
//
//  Created by Mohamad el mohamady Ghonem on 9/28/20.
//  Copyright © 2020 Mohamad el mohamady Ghonem. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {
   
    

    @IBOutlet weak var inputTF: UITextField!
    @IBOutlet weak var submitBtn: UIButton!
    var presenter: SecondStringArray? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func submitTapped(_ sender: Any) {
        
        if !inputTF.text!.isEmpty {
            presenter?.SubmitBtnPressed(title: inputTF.text!)
        }
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
