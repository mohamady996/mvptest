//
//  FirstViewController.swift
//  MVPTesting
//
//  Created by Mohamad el mohamady Ghonem on 9/28/20.
//  Copyright © 2020 Mohamad el mohamady Ghonem. All rights reserved.
//

import UIKit

protocol FirstViewProtocol: UIViewController{
    func assignData(strings: [String] )
}

class FirstViewController: UIViewController, FirstViewProtocol {

    @IBOutlet weak var tableView: UITableView!
    var strings = [String]()
    var presenter: FirstStringArray? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter = FirstPresenter(with: self)
        // Do any additional setup after loading the view.
    }

    @IBAction func addPressed(_ sender: UIButton) {
        presenter!.addBtnPressed()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        presenter?.viewDidLoad()
        
    }
    
    func assignData(strings: [String] ){
        self.strings = strings
        tableView.reloadData()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension FirstViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return strings.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let  cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "Cell")
        
        cell.textLabel?.text = strings[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            // handle delete (by removing the data from your array and updating the tableview)
            presenter?.deleteBtnPressed(index: indexPath.row)
        }
    }
}

//extension FirstViewController: FirstViewProtocol{
//    var nav: UINavigationController {
//        return self.navigationController!
//    }
//
//    func assignData(strings: [String] ){
//        self.strings = strings
//        tableView.reloadData()
//    }
//}
