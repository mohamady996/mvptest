//
//  SecondPresenter.swift
//  MVPTesting
//
//  Created by Mohamad el mohamady Ghonem on 9/28/20.
//  Copyright © 2020 Mohamad el mohamady Ghonem. All rights reserved.
//

import Foundation
import UIKit

protocol SecondStringArray{
    func SubmitBtnPressed(title: String)
    init(with view: SecondViewController)
}

class SecondPresenter{
    var view: SecondViewController
    // Pass something that conforms to PresenterView
    
    required init(with view: SecondViewController) {
        self.view = view
    }
}

extension SecondPresenter: SecondStringArray{
    func SubmitBtnPressed(title: String) {
        let defaults = UserDefaults.standard
        var myarray = defaults.stringArray(forKey: "SavedStringArray") ?? [String]()
        myarray.append(title)
        defaults.set(myarray, forKey: "SavedStringArray")

        self.view.navigationController?.popViewController(animated: true)
       }
}
