//
//  FirstPresenter.swift
//  MVPTesting
//
//  Created by Mohamad el mohamady Ghonem on 9/28/20.
//  Copyright © 2020 Mohamad el mohamady Ghonem. All rights reserved.
//

import Foundation
import UIKit


protocol FirstStringArray{
    func viewDidLoad()
    func addBtnPressed()
    func deleteBtnPressed(index: Int)
    init(with view: FirstViewController)
}

class FirstPresenter{
    var view: FirstViewProtocol
    // Pass something that conforms to PresenterView
    
    required init(with view: FirstViewController) {
        self.view = view
    }
    
    func getStringArray() -> [String] {
        let defaults = UserDefaults.standard
        let myarray = defaults.stringArray(forKey: "SavedStringArray") ?? [String]()
        return myarray
    }
    
    
}

extension FirstPresenter: FirstStringArray{
    
    func viewDidLoad(){
        var strings = [String]()
        strings = getStringArray()
        view.assignData(strings: strings)
    }
    
    func addBtnPressed() {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "second") as? SecondViewController
        vc?.presenter = SecondPresenter(with: vc!)
        view.navigationController!.pushViewController(vc!, animated: true)
    }
    
    func deleteBtnPressed(index: Int) {
        let defaults = UserDefaults.standard
        var myarray = defaults.stringArray(forKey: "SavedStringArray") ?? [String]()
        myarray.remove(at: index)
        defaults.set(myarray, forKey: "SavedStringArray")
        view.assignData(strings: myarray)
    }
}
